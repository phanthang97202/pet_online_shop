﻿namespace OnlineShop.Application.UseCases.Auth
{
    public class TokenPresenter
    {
        public string AccessToken { get; set; }
        public string RefreshToken { get; set; }
    }
}
